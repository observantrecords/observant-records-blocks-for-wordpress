import { registerBlockType } from '@wordpress/blocks';

const INLINE_LIST = 'observant-records/inline-list';

registerBlockType( INLINE_LIST, {
    title: 'Inline list',
    icon: 'editor-ul',
    category: 'text',
    edit: ( props ) => {

    },
    save: () => {

    },
} );

export { INLINE_LIST };
