import { registerBlockType } from '@wordpress/blocks';
import { postList } from '@wordpress/icons';
import { withSelect } from '@wordpress/data';
import { InspectorControls } from '@wordpress/block-editor';
import { PanelBody, PanelRow, __experimentalNumberControl as NumberControl, SelectControl } from '@wordpress/components';
import { withState } from '@wordpress/compose';

const NEWS_CARDS = 'observant-records/news-cards';

registerBlockType( NEWS_CARDS, {
    title: 'Latest posts (Cards)',
    icon: postList,
    category: 'widgets',
    attributes: {
        postsToShow: {
            type: 'number',
            default: 3
        },
    },
    edit: withSelect( ( select, props ) => {
        return {
            posts: select( 'core' ).getEntityRecords( 'postType', 'post' ),
        };
    } )( ( props ) => {
        const { attributes: { postsToShow }, className, setAttributes, posts } = props;

        const onChangePostToShow = ( value ) => {
            setAttributes( { postsToShow: parseInt( value )  } );
        };

        if ( !posts ) {
            return 'Loading ...';
        }

        if ( posts && posts.length === 0 ) {
            return 'No posts.';
        }

        const postListAll = posts.map( ( post ) => <li>
            <a href={ post.link } className={ className }>
                { post.title.rendered }
            </a>
        </li> );

        const postList = ( postsToShow > 0 ) ? postListAll.slice( 0, postsToShow ) : postListAll;

        return <div>
            <InspectorControls>
                <PanelBody title={ "Settings" }>
                    <PanelRow>
                        <NumberControl
                            label={ "Number of posts"}
                            onChange={ onChangePostToShow }
                            value={ postsToShow }
                        />
                    </PanelRow>
                </PanelBody>
            </InspectorControls>
            <ul className={ className }>
                { postList }
            </ul>
        </div>
    } ),
    save: () => {
        return null;
    },
} );

export { NEWS_CARDS };
