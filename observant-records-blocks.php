<?php
/**
 * Plugin Name: Observant Records Blocks
 * Plugin URI: https://bitbucket.org/observantrecords/observant-records-blocks-for-wordpress
 * Description: This custom plugin creates custom blocks for the Observant Records web site.
 * Version: 1.0.2
 * Author: Greg Bueno
 * Author URI: http://vigilantmedia.com
 * License: MIT
 */

namespace ObservantRecords\WordPress\Plugins\Blocks;

define( __NAMESPACE__ . '\PLUGIN_BASE_PATH', __FILE__ );
define( __NAMESPACE__ . '\PLUGIN_BASE_URL', plugins_url( __FILE__ ) );

if (!function_exists( __NAMESPACE__ . '\\autoload' )) {
    function autoload( $class_name )
    {
        $class_name = ltrim($class_name, '\\');
        if ( strpos( $class_name, __NAMESPACE__ ) !== 0 ) {
            return;
        }

        $class_name = str_replace( __NAMESPACE__, '', $class_name );

        $path = plugin_dir_path(__FILE__) . '/lib' . str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';

        require_once($path);
    }
}

spl_autoload_register(__NAMESPACE__ . '\\autoload');

register_activation_hook(__FILE__, array('ObservantRecords\WordPress\Plugins\Blocks\Setup', 'activate'));
register_deactivation_hook(__FILE__, array('ObservantRecords\WordPress\Plugins\Blocks\Setup', 'deactivate'));

Setup::init();
