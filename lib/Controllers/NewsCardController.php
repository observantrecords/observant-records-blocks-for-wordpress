<?php


namespace ObservantRecords\WordPress\Plugins\Blocks\Controllers;


use const ObservantRecords\WordPress\Plugins\Blocks\PLUGIN_BASE_PATH;

class NewsCardController
{

    public function __construct() {

    }

    public static function init() {

        add_action( 'init', array( __CLASS__, 'registerNewsCardBlock' ) );

    }

    public static function getNewsCards( $block_attributes, $content ) {

        $output = null;

        if ( $block_attributes['postsToShow'] === null || $block_attributes['postsToShow'] === '' ) {
            $block_attributes['postsToShow'] = 3;
        }

        $recent_posts = wp_get_recent_posts( array(
            'numberposts' => $block_attributes['postsToShow'],
            'post_status' => 'publish',
        ) );

        $cards = null;
        if ( count( $recent_posts ) > 0 ) {

            $card_callback = function ( $post ) {
                $permalink = get_permalink( $post['ID'] );
                $thumbnail = get_the_post_thumbnail( $post['ID'], 'small', array(
                    'class' => 'card-img-top'
                ) );
                $excerpt = get_the_excerpt( $post['ID'] );
                $card = <<< CARD
    <div class="card min-vw-33">
        <a href="$permalink">
            $thumbnail
        </a>
        <div class="card-body">
            <h4 class="card-title">
                <a href="$permalink">
                    {$post['post_title']}
                </a>
            </h4>
            <div class="card-text">
                    $excerpt
            </div>
        </div>
    </div>

CARD;
                return $card;
            };

            $cardPosts = array_map( $card_callback, $recent_posts );
            $cards .= implode( "\n", $cardPosts );
            $cardDebug = print_r( $recent_posts, true );

        }

        $output .= <<< OUTPUT
<div class="card-deck wp-block-observant-records-news-cards">
    $cards
</div>
OUTPUT;


        return $output;
    }

    public static function registerNewsCardBlock() {
        $asset_file = include( dirname( PLUGIN_BASE_PATH ) . '/build/index.asset.php');

        wp_register_script(
            'observant-records-news-card-block',
            plugins_url( 'build/index.js', PLUGIN_BASE_PATH ),
            $asset_file['dependencies'],
            $asset_file['version']
        );

        register_block_type( 'observant-records/news-cards', array(
            'editor_script' => 'observant-records-news-card-block',
            'render_callback' => array( __CLASS__, 'getNewsCards' ),
        ) );

    }

}