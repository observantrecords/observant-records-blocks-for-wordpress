<?php


namespace ObservantRecords\WordPress\Plugins\Blocks;


use ObservantRecords\WordPress\Plugins\Blocks\Controllers\NewsCardController;

class Setup
{
    /**
     * Setup constructor.
     */
    public function __construct() {

    }

    /**
     * init
     *
     * init() registers WordPress actions and filters to setup the plugin.
     */
    public static function init() {

        NewsCardController::init();

    }

    /**
     * activate
     *
     * Use this method with register_activation_hook().
     */
    public static function activate() {

    }

    /**
     * deactivate
     *
     * Use this method with register_deactivation_hook.
     */
    public static function deactivate() {

    }

    /**
     * uninstall
     *
     * Use this method with register_uninstall_hook().
     */
    public static function uninstall() {

    }

}